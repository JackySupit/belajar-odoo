# -*- coding: utf-8 -*-
from odoo import http

class baru6(http.Controller):
    @http.route('/baru6/baru6/', auth='public')
    def index(self, **kw):
        return "Hello, world"

    @http.route('/baru6/baru6/objects/', auth='public')
    def list(self, **kw):
        return http.request.render('baru6.listing', {
            'root': '/baru6/baru6',
            'objects': http.request.env['baru6.baru6'].search([]),
        })
#
    @http.route('/baru6/baru6/objects/<model("baru6.baru6"):obj>/', auth='public')
    def object(self, obj, **kw):
        return http.request.render('baru6.object', {
            'object': obj
        })