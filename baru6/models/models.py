# -*- coding: utf-8 -*-
from odoo import models, fields, api


class baru6(models.Model):
    _name = 'baru6.baru6'

    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()

    @api.depends('value')
    def _value_pc(self):
        self.value2 = float(self.value) / 100

class kursus(models.Model):
    _name = 'baru6.kursus'
    _auto = True

    name = fields.Char(string="Judul", required=True)
    description = fields.Text()


class mata_pelajaran(models.Model):
    _name = 'baru6.mata_pelajaran'
    _auto = True
    _rec_name = 'nama'

    kode = fields.Char(string="kode", required=True)
    nama = fields.Char(string="nama", required=True)
    description = fields.Text()

