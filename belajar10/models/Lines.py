from odoo import models, fields, api

class BuyLine(models.Model):
    _name = "belajar10.buy.line"

    buy_id = fields.Many2one('belajar10.buy', 'Inventory')
    product_id = fields.Many2one('belajar10.product', 'Product')
    qty = fields.Float('Quantity', default=0)
    price = fields.Float(string='Price')
    subtotal = fields.Float(string='Subtotal', readonly=True)
    
    
    @api.onchange('qty', 'price')
    def _onchange_subtotal(self):
        if not self.price: self.price = 0 
        if not self.qty: self.qty= 0
        self.subtotal = self.price * self.qty
        
class Product(models.Model):
    _name = "belajar10.product"

    name = fields.Char(string="Nama", size=50, required=True)

class Buy(models.Model):
    _name = 'belajar10.buy'

    name = fields.Char(string="Nama", size=50, required=True)
    total = fields.Float(string='Total')
    line_ids = fields.One2many('belajar10.buy.line', 'buy_id', string='Inventories')

    @api.onchange('line_ids')
    def _onchange_line_ids(self):
        self.total = 0;
        for rec in self.line_ids:
            self.total += (rec.qty * rec.price)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Buy, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if view_type == 'form':
            res['arch'] = """
				<form string="Buy Transaction Form">
					<sheet>
						<group>
							<group>
								<field name="name" />
							</group>
						</group>
						<notebook>
                            <page string="Order Lines">
                                <field name="line_ids" mode="tree,kanban">
                                    <tree string="Order Lines" editable="bottom">
                                        <field name="product_id"/>
                                        <field name="qty"/>
                                        <field name="price"/>
                                        <field name="subtotal"/>
                                    </tree>
                                </field>
                            </page>
						</notebook>
						<group>
							<field name="total" />
						</group>
					</sheet>
				</form>
			"""
        elif view_type == 'tree':
            res['arch'] = """
				<tree string="Buy List">
					<field name="name" />
					<field name="total" widget="float" type="char" />
				</tree>
			"""

        from lxml import etree
        doc = etree.XML(res['arch'])
        View = self.env['ir.ui.view'].sudo()
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res