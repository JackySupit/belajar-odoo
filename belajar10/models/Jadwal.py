# -*- coding: utf-8 -*-
import datetime
from odoo import models, api, fields
import logging
_logger = logging.getLogger(__name__)

class Jadwal(models.Model):
    _name = "belajar10.jadwal"
    _rec_name = "nomor"
    
    jam = fields.Datetime(required=True)
    jam_keberangkatan = fields.Datetime(required=True)
    jam_kedatangan = fields.Datetime(required=True)
    
    bus_id = fields.Many2one("belajar10.bus", "Bus", required=True)
    sopir_id = fields.Many2one("res.partner", "Sopir")
    penumpang_ids = fields.Many2many("belajar10.penumpang", String="Penumpang")
    
    nomor = fields.Char("Nomor", help="Nomor Jadwal - otomatis", default="New", readonly=True)
    status = fields.Selection([
        ("ok", "OK"),
        ("delay", "Delay"),
        ("done", "Done"),
        ("cancel", "Cancelled"),
        ])

    price = fields.Float(default=50000)
    
    @api.model
    def create(self, vals):
        vals.update(
            {'nomor': self.env["ir.sequence"].next_by_code("belajar10.sequence.jadwal")}
            )

        self.update_saldo_penumpang(vals)
        return super(Jadwal, self).create(vals)
    
    
    @api.multi
    def write(self, vals):
        nomor3 = self.browse(3).nomor
        nama_penumpang2 = self.env["belajar10.penumpang"].browse(2).nama
#         import ipdb; ipdb.set_trace()
        if((not self.nomor) or (self.nomor == "New")):
            nomor = self.env["ir.sequence"].next_by_code("belajar10.sequence.jadwal")
            vals.update(
                {'nomor': nomor}
                )

        self.update_saldo_penumpang(vals)
        record = super(Jadwal, self).write(vals)
        return record


    def update_saldo_penumpang(self, vals):
        old_penumpang_ids = self.penumpang_ids.mapped('id')

        if ('penumpang_ids' in vals) and (vals['penumpang_ids']):
            new_penumpang_ids = vals['penumpang_ids'][0][2]
        else:
            new_penumpang_ids = []

        price = 0
        price_lama = 0
        price_baru = 0
        if('price' in vals):
            if not vals['price']:
                vals['price'] = 50000
            price = vals['price']
            price_baru = price
            price_lama = self.price
        else:
            price = self.price

        for penumpang_id in old_penumpang_ids:
            if penumpang_id not in new_penumpang_ids: #ini adalah penumpang yang dihapus
                master_penumpang = self.env['belajar10.penumpang'].browse(penumpang_id)
                master_penumpang.saldo += price

        for penumpang_id in new_penumpang_ids:
            if penumpang_id not in old_penumpang_ids: # ini untuk penumpang yang baru dimasukkan di mode edit
                master_penumpang = self.env['belajar10.penumpang'].browse(penumpang_id)
                master_penumpang.saldo -= price




    #set([1, 2]).symmetric_difference(set([2, 3]))
    #set(vals['penumpang_ids'][0][2]).symmetric_difference(set([2, 3]))
    # self.penumpang_ids.mapped('id')   #old value
    # vals['penumpang_ids'][0][2]       #new value
    # set(self.penumpang_ids.mapped('id')).symmetric_difference(set(vals['penumpang_ids'][0][2]))

    
    @api.onchange("jam_keberangkatan")
    def _onchange_jam_keberangkatan(self):
        if self.jam_kedatangan and self.jam_keberangkatan:
            deltime = datetime.datetime.strptime(self.jam_kedatangan,"%Y-%m-%d %H:%M:%S") - datetime.datetime.strptime(self.jam_keberangkatan,"%Y-%m-%d %H:%M:%S")
            if deltime.total_seconds() > 600:
                self.state = 'delay'
            else:
                self.state = 'ok'