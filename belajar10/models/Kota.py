# -*- coding: utf-8 -*-
import datetime
from odoo import models, api, fields, _
import logging
_logger = logging.getLogger(__name__)

class Kota(models.Model):
    _name = "belajar10.kota"
    _rec_name = "nama"

    nama = fields.Char(string="Nama", size=50, required=True)
    negara_id = fields.Many2one("belajar10.negara", string="Negara", required=True)