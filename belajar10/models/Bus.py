# -*- coding: utf-8 -*-
import datetime
from odoo import models, api, fields, _
import logging
_logger = logging.getLogger(__name__)

class Bus(models.Model):
    _name = "belajar10.bus"
    _rec_name = "nama"

    kode = fields.Char(size=50, required=True)
    nama = fields.Char(size=50, required=True)
    kapasitas = fields.Float()
    foto = fields.Binary("Foto Bus")
    status = fields.Selection([
        ("ok", _("OK")),
        ("out_of_order", _("Out Of Order")),
        ("maintenance", _("Maintenance")),
        ], default="ok")
    warna = fields.Selection([
        ("merah", _("Merah")),
        ("kuning", _("Kuning")),
        ("hijau", _("Hijau")),
        ], default="merah")

    jadwal_ids = fields.One2many("belajar10.jadwal", "bus_id", "Jadwal")

    doctor = fields.Boolean(default=True)
    patient = fields.Boolean(default=False)
    supplier = fields.Boolean(default=True)
    customer = fields.Boolean(default=False)


class BusSetMaintenanceWizard(models.TransientModel):
    _name="belajar10.bus.set.maintenance.wizard"

    bus_ids = fields.Many2many("belajar10.bus", String="Bus")

    _logger.warning("ACT MAINTENANCE!! DEFAULT DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD")

    @api.model
    def default_get(self, fields):
        _logger.warning("ACT MAINTENANCE!! DEFAULT AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
        res = super(BusSetMaintenanceWizard, self).default_get(fields)
        return {
                'bus_ids':[[6,False, self.env["belajar10.bus"].search([("status", "!=", "maintenance")]).ids ]],
            }

    @api.multi
    def act_maintenance(self):
        _logger.warning("ACT MAINTENANCE!! SET BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")
        for bus_id in self.bus_ids:
            _logger.warning("ACT MAINTENANCE!! SET bus_id: " + str(bus_id.id) + " | state lama: " + str(bus_id.status));
            _logger.warning("ACT MAINTENANCE!! SET EEEEEEEEEEEEEEEEEEEEEEE ")
            bus_id.status = 'maintenance'


    @api.model
    def create(self, vals):
        _logger.warning("ACT MAINTENANCE!! CREATE CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC")
        return super(BusSetMaintenanceWizard, self).create(vals)
