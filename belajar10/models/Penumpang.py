# -*- coding: utf-8 -*-
import datetime
from odoo import models, api, fields, _
import logging
_logger = logging.getLogger(__name__)

class Penumpang(models.Model):
    _name = "belajar10.penumpang"
    _rec_name = "nama"

    _logger.info(_("START PENUMPANG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"))

    @api.model
    def _get_create_date(self):
        dt = datetime.datetime.now().date()
        _logger.info("Date: " + str(dt) + " ||||||||||||||||||||||||||||||||||||||||||")
        return dt

    @api.model
    def _get_create_time(self):
        jammenit = datetime.datetime.now().hour + (datetime.datetime.now().minute / 60)
        _logger.info("jammenit: " + str(jammenit) + " ||||||||||||||||||||||||||||||")
        return jammenit

    nama = fields.Char(size=50, required=True)
    tgl_lahir = fields.Date()
    status_penumpang = fields.Selection([("umum", "Umum"), ("member", "Member")], default="umum")
    status_menikah = fields.Selection([("menikah", "Menikah"), ("belum_menikah", "Belum Menikah")], default="belum_menikah")

    berat = fields.Float()
    tinggi = fields.Float()
    nomor = fields.Char(size=32, required=True)

    tgl_create_date = fields.Date(readonly=1, default=_get_create_date)
    time_create_date = fields.Float(readonly=1, default=_get_create_time)
    age = fields.Integer("Age", compute="_compute_age", store=False)

    saldo = fields.Float("Saldo", readonly=True)
    jam_lahir = fields.Float("Jam Lahir")

    @api.depends("tgl_lahir")
    def _compute_age(self):
        for rec in self:
            tgl_lahir = rec.tgl_lahir
            if tgl_lahir:
                rec.age = (
                        datetime.datetime.now().year -
                        datetime.datetime.strptime(tgl_lahir,"%Y-%m-%d").year
                    )

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Penumpang, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if(view_type=="form"):
            satu = _("do you want to go home now?")
            _logger.warning(satu)
            satu = _("would you like to have some ice cream?")
            _logger.warning(satu)
            satu = _("i think i love you")
            _logger.warning(satu)
            satu = _("you never walk alone")
            _logger.warning(satu)

        return res;


    @api.model
    def create(self, vals):
        _logger.warning("On Create aaaaaaaa")
        return super(Penumpang, self).create(vals)
    
    
    @api.multi
    def write(self, vals):
        _logger.warning("On Write aaaaaaaa")
        return super(Penumpang, self).write(vals)
    
