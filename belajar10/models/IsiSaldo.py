# -*- coding: utf-8 -*-
import datetime
from odoo import models, api, fields, _
import logging
_logger = logging.getLogger(__name__)

class IsiSaldo(models.Model):
    _name = "belajar10.isi_saldo"
    _rec_name = "penumpang_id"

    penumpang_id = fields.Many2one("belajar10.penumpang", string="Penumpang", required=True)
    jumlah = fields.Float(string="Jumlah", required=True, default=25000)

    @api.model
    def create(self, values):
        record = super(IsiSaldo, self).create(values)
        penumpang = self.env['belajar10.penumpang'].browse(record.penumpang_id.id)
        penumpang.saldo = penumpang.saldo + record.jumlah
        # import ipdb; ipdb.set_trace()
        return record