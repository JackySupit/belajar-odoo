# -*- coding: utf-8 -*-
import datetime
from odoo import models, api, fields, _
import logging
_logger = logging.getLogger(__name__)

class Negara(models.Model):
    _name = "belajar10.negara"
    _rec_name = "nama"
    
    kode = fields.Char(size=2, required=True)
    nama = fields.Char(string="Nama", size=50, required=True)

    @api.model
    def create(self, vals):
        vals['nama'] += ' (injected)'
        return super(Negara, self).create(vals)
