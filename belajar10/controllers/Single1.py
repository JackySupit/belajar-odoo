# -*- coding: utf-8 -*-
from odoo import http
import logging
_logger = logging.getLogger(__name__)

class Single1(http.Controller):
    @http.route('/single1', type='http', auth='public', website=True)
    def render_single1(self, **kwargs):
        vData = {"nama": "Joko", "jenis_kelamin": "Laki-laki"}
        vData.update(kwargs)
        # import ipdb;ipdb.set_trace() #ctrl+D untuk keluar dari IPDB
        return http.request.render('belajar10.single1_page', vData)

    @http.route('/single2', type='http', auth='public', website=True)
    def render_single2(self, **kwargs):
        return "DUA"

    @http.route('/single3', type='http', auth='public', website=True)
    def render_single3(self, **kwargs):
        vData = {"nama": "Joko", "jenis_kelamin": "Laki-laki"}
        vData.update(kwargs)
        # import ipdb;ipdb.set_trace()
        return http.request.render('belajar10.single1_page', vData)

    @http.route('/single1_detail', type='http', auth='public', website=True)
    def render_single1_detail(self, **kwargs):
        vData = {"nama": "Joko", "jenis_kelamin": "Laki-laki"}
        vData.update(kwargs)

        # companies = self.env['res.company'].search([])
        m_partner = http.request.env['res.partner'].sudo().search([])
        vData.update({"m_partner": m_partner})
        return http.request.render('belajar10.single1_detail_page', vData)

    @http.route('/single1/detail/edit/<id_model>', type='http', auth='public')
    def partner_edit(self, id_model, **args):
        row_partner = http.request.env['res.partner'].sudo().search([('id', '=', id_model)])
        name = row_partner.name
        return 'edit name: ' + name

    @http.route('/single1/create_kota', type='http', auth='public', website=True)
    def render_single1_create_kota(self, **kwargs):
        vData = {"nama": "Kota Baru 1", "negara_id": 1}
        return str(vData);

    @http.route('/warna/$1', type='http', auth='public', website=True)
    def render_warna(self, **kwargs):
        str = "apa? <span class='btn btn-light btn-halo'>HALO</span> " + str([kwargs])
        return str;

    @http.route('/single1/create', type='http', auth="public", website=True)
    def create_create(self, **kwargs):
        crm_kota = http.request.env['belajar10.kota'].sudo().create({
            'nama':"kota baru 1",
            'negara_id': 1
        })
        # return http.request.redirect("/single1/detail/%s" % slug(crm_kota))
        return str(crm_kota)





    @http.route('/single1/baru1/<brand_id>', auth='public', type='http', website=True)
    def baru1(self, brand_id, **kwargs):
        import ipdb;ipdb.set_trace()
        return "BARU1"
        # cr = http.request._cr
        # res = utils.get_models(cr, int(brand_id))
        # return {'models': res, }

    @http.route('/buah/<nama_buah>', auth='public', type='http', website=True)
    def buah(self, nama_buah, **kwargs):
        return "Nama Buah: " + nama_buah
        # cr = http.request._cr
        # res = utils.get_models(cr, int(brand_id))
        # return {'models': res, }


    @http.route('/single1/satu/<model("belajar10.kota"):kota>', type='http', auth="public", website=True)
    def show_satu_crm_kota(self, kota, **kwargs):
        return "single 1 kota"

    # @http.route('/single1/json1', type='http', auth='public', website=True)
    @http.route('/single1/<slug>', type='http', auth='public')
    def render_single1_slug(self, **kwargs):
        vData = {"nama": "Joko", "jenis_kelamin": "Laki-laki"}
        vData.update(kwargs)

        # companies = self.env['res.company'].search([])
        m_partner = http.request.env['res.partner'].sudo().search([])
        vData.update({"m_partner": m_partner})
        return str(vData)


    # @http.route('/single1/json1', type='http', auth='public', website=True)
    @http.route('/single1/json1', type='http', auth='public')
    def render_single1_json1(self, **kwargs):
        vData = {"nama": "Joko", "jenis_kelamin": "Laki-laki"}
        vData.update(kwargs)

        # slug1 = slug("ini kata kata buat di slug satu")
        # slug2 = slug("2 2 - 2=3 ini kata kata buat di slug satu")
        # vData.update({'slug1':slug1, 'slug2': slug2})

        # companies = self.env['res.company'].search([])
        m_partner = http.request.env['res.partner'].sudo().search([])
        vData.update({"m_partner": m_partner})
        return str(vData)

    @http.route('/single1/json4', type='http', auth='public')
    def render_single1_json4(self, **args):
        _logger.info('The controller | type : http |  single/json4 is called.')
        return '{"response": "OK"}'


    @http.route('/single1/json5', type='http', auth='public')
    def render_single1_json5(self, **args):
        _logger.info('The controller | type : http |  single/json4 is called.')
        return '{"response": "OK"}'


