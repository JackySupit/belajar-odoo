# -*- coding: utf-8 -*-
from odoo import http

class Api(http.Controller):
    @http.route('/api1', type='http', auth='public', website=True)
    def render_api1(self, **kwargs):
        return "API Satu"

    @http.route('/api2', type='http', auth='public', website=True)
    def render_api2(self, **kwargs):
        vData = {"nama": "Joko", "jenis_kelamin": "Laki-laki"}
        vData.update(kwargs)
        return http.request.render('belajar10.single1_page', vData)

    @http.route('/api3', type='http', auth='public', website=True)
    def render_api3(self, **kwargs):
        vData = {"nama": "Joko", "jenis_kelamin": "Laki-laki"}
        vData.update(kwargs)

        m_partner = http.request.env['res.partner'].sudo().search([])
        vData.update({"m_partner": m_partner})
        return http.request.render('belajar10.single1_detail_page', vData)


    @http.route('/api4', type='http', auth='public', website=True)
    def render_api1(self, **kwargs):
        x = "Hello Api4"
        y = ["satu"]
        y.append(x + " - 1")
        y.append(x + " - 2")
        y.append(x + " - 3")
        z = ', '.join(y)
        print(y)
        return z

    @http.route('/negara/new', type='http', auth='public', website=True)
    def render_api1(self, **kwargs):
        Negara = http.request.env['belajar10.negara']
        # vals = [2]
        # vals["kode"] = "BD"
        # vals["nama"] = "Brunei Darussalem"
        vals = {
            'kode': 'ZE',
            'nama': 'New Zealand',
        }
        records = Negara.create(vals)
        # print(x)

        kode = ''
        nama = ''
        for record in records:
            kode = record.kode
            nama = record.nama


        return "OK, SUKSES: kode: {}, nama: {}".format(kode, nama)