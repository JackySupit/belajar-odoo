# -*- coding: utf-8 -*-
{
    'name': "Belajar 10",
    'summary': """Belajar Odoo 10""",
    'description': """
        Open Academy module for managing trainings:
            - training courses
            - training sessions
            - attendees registration
    """,

    'author': "Jeksu",
    'website': "http://www.jeksu.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'website', 'report', 'report_xlsx'],

    # always loaded
    'data': [
        'security/transport_security.xml',
        'security/ir.model.access.csv',
        'data/urutan_jadwal.xml',
        'views/belajar10_template.xml',
        'views/bus.xml',
        'views/penumpang.xml',
        'views/jadwal.xml',
        'views/negara.xml',
        'views/kota.xml',
        'views/single1.xml',
        'views/isi_saldo.xml',
        'wizard/bus_set_maintenance_wizard.xml',
        'report/report_penumpang.xml',
        'views/util.xml',
        'views/lines.xml',
        'views/menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
    'installable': True,
    'application': True,
}