from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
from docutils.nodes import title

class PenumpangXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, models):
        for obj in models:
            nama = obj.nama
            status = "SATU" if True else "DUA"
            status2 = "SATU" if False else "DUA"
            # One sheet by partner
            sheet = workbook.add_worksheet(nama[:31])
            bold = workbook.add_format({'bold': True})
            sheet.write(0, 0, nama, bold)
            sheet.write(0, 1, status, bold)
            sheet.write(0, 2, status2, bold)
            sheet.write(0, 3, "AA" if True else "BB", bold)
            sheet.write(0, 4, "AA" if False else "BB", bold)

PenumpangXlsx('report.list.penumpang2.xlsx','belajar10.penumpang')

