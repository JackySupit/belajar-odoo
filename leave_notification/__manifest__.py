# -*- coding: utf-8 -*-
##############################################################################
#    Jacky supit
##############################################################################
{
    'name': 'Leave Notification',
    'summary': """Employee Leave Status Notification""",
    'version': '0.1.0',
    'description': """Send notification by email to selected persons""",
    'author': 'Jacky supit',
    'company': 'Juke cloud',
    'website': 'http://www.jeksu.com',
    "category": "Generic Modules/Human Resources",
    'depends': ['mail', 'hr_holidays'],
    'license': 'AGPL-3',
    'data': [
         'views/template_email_notification.xml',
         'views/menu.xml',
             ],
    'demo': [],
    'images': ['static/description/banner.jpg'],
    'installable': True,
    'application': True,
    'auto_install': False,

}


