# -*- coding: utf-8 -*-
from odoo import models, api, fields
import logging
_logger = logging.getLogger(__name__)

class GroupNotifLeave(models.Model):
    _name = "leave_notification.group_notif_leave"
    department_id = fields.Many2one('hr.department', string="Pilih Department")

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(GroupNotifLeave, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if(view_type=="tree"):
            res['arch'] = """
                <tree string="Course Tree">
                    <field name="department_id" context="{'tree_view_ref': 'department_id.name'}"/>
                </tree>
            """

        from lxml import etree
        doc = etree.XML(res['arch'])
        View = self.env['ir.ui.view'].sudo()
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res


    @api.onchange('department_id')
    def on_change_department_id(self):
        rows = self.search([]).mapped('department_id').mapped('id')
        selected_ids = rows
        domain = [
            ('id', 'not in', selected_ids)
        ]
        result = {
            'domain': {
                'department_id': domain,
            },
        }
        return result


class LeaveNotification(models.Model):
    _inherit = 'hr.holidays'

    @api.multi
    def write(self, values):
        result = super(LeaveNotification, self).write(values)
        ff = open('/opt/odoo/_temp/log.txt', 'wb')

        if(result):
            for holiday in self:
                if holiday.state in ['draft']:
                    a = "PENGAJUAN CUTI DIBUAT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
                    ff.write(a)
                    for record in self:
                        # hrd_ids = self.env['hr.employee'].search(['department_id','=',10]) #ini mengembalikan records
                        # hrd_ids = self.env['hr.employee'].search([]).mapped('email') #ini mengembalikan array email
                        # depts = self.env['leave_notification.group_notif_leave'].search([]) #ini mengembalikan records
                        dept_ids = self.env['leave_notification.group_notif_leave'].search([]).mapped('department_id').mapped('id') #ini mengembalikan records
                        # import ipdb; ipdb.set_trace()
                        # for dept_id in dept_ids:
                        #     ff.write("department_id: " + str(dept_id))
                        rec_hrds = self.env['hr.employee'].search([('department_id', 'in', dept_ids)]) #ini mengembalikan array email
                        for rec_hrd in rec_hrds:
                            template_obj = self.env['mail.mail']
                            template_data = {}
                            template_data["subject"] = "Ada Pengajuan Cuti Baru"
                            # template_data["email_from"] = "jsupit@gmail.com"
                            template_data["email_to"] = rec_hrd.work_email
                            template_data['body_html'] = """
                                <div style="font-family: 'Lucica Grande', Ubuntu, Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: #FFF; ">
                                    <p>
                                        Dear <strong>%s,</strong>
                                        Ada permintaan cuti dari <strong>%s.</strong>
                                    </p>
                                    <p><strong>Description:</strong></p>
                                    <p>Leave : %s </p>
                                    <p>No of Days : <strong>%s</strong></p>
                                    <p>Leave From : %s </p>
                                    <p>Leave UpTo : %s </p>
                                    <p>Untuk melihat detail dari pengajuan tersebut, silahkan klik link di bawah ini:</p>
                                    <p>
                                        <a href="http://localhost:8069/web?db=%s#id=%s&view_type=form&model=hr.holidays">Link</a>
                                    </p>
                                </div>
                            """ % (rec_hrd.name
                                    , record.employee_id.name
                                    , record.holiday_status_id.name
                                    , record.number_of_days_temp
                                    , record.date_from
                                    , record.date_to
                                    , record.env.cr.dbname, record.id)
                            kirim_email = template_obj.create(template_data).send()
                            a = "kirim email Permintaan Dibuat to: %s (%s)" % (rec_hrd.work_email, template_data["subject"])
                            ff.write(a + "status: " + str(kirim_email) + "\n")

                    # ir_model_data = self.pool.get('ir.model.data')
                    # template_id = ir_model_data.get_object_reference('leave_notification', 'template_leave_approved')[1]
                    # self.pool.get('mail.template').send_mail(template_id, ids[0], force_send=True, context=context)
                else:
                    ff.write('holiday.state: ' + holiday.state)
        return result

    @api.multi
    def action_approve(self):
        result = super(LeaveNotification, self).action_refuse()
        ff = open('/opt/odoo/_temp/log.txt', 'wb')

        if(result):
            a = "PENGAJUAN CUTI DITERIMA ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
            ff.write(a)
            for record in self:
                # hrd_ids = self.env['hr.employee'].search(['department_id','=',10]) #ini mengembalikan records
                # hrd_ids = self.env['hr.employee'].search([]).mapped('email') #ini mengembalikan array email
                template_obj = self.env['mail.mail']
                template_data = {}
                template_data["subject"] = "Pengajuan Cuti Anda Diterima"
                # template_data["email_from"] = "_default"
                template_data["email_to"] = record.employee_id.work_email
                template_data['body_html'] = """
                    <div style="font-family: 'Lucica Grande', Ubuntu, Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: #FFF; ">
                        <p>
                            Dear <strong>%s,</strong>
                            Selamat, Permintaan Cuti Anda <strong>Diterima</strong> 
                        </p>
                        <p><strong>Description:</strong></p>
                        <p>Leave : %s </p>
                        <p>No of Days : <strong>%s</strong></p>
                        <p>Leave From : %s </p>
                        <p>Leave UpTo : %s </p>
                        <p>Untuk melihat detail dari pengajuan tersebut, silahkan klik link di bawah ini:</p>
                        <p>
                            <a href="http://localhost:8069/web?db=%s#id=%s&view_type=form&model=hr.holidays">Link</a>
                        </p>
                    </div>
                """ % (record.employee_id.name
                        , record.holiday_status_id.name
                        , record.number_of_days_temp
                        , record.date_from
                        , record.date_to
                        , record.env.cr.dbname, record.id)
                kirim_email = template_obj.create(template_data).send()
                a = "kirim email Permintaan Diterima to: %s  (subject: %s):" % (record.employee_id.work_email, template_data["subject"])
                ff.write(a + "status: " + str(kirim_email) + "\n")

            # ir_model_data = self.pool.get('ir.model.data')
            # template_id = ir_model_data.get_object_reference('leave_notification', 'template_leave_approved')[1]
            # self.pool.get('mail.template').send_mail(template_id, ids[0], force_send=True, context=context)

        return result

    @api.multi
    def action_refuse(self):
        result = super(LeaveNotification, self).action_refuse()
        ff = open('/opt/odoo/_temp/log.txt', 'wb')

        if(result):
            a = "PENGAJUAN CUTI DITOLAK ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
            ff.write(a)
            for record in self:
                # hrd_ids = self.env['hr.employee'].search(['department_id','=',10]) #ini mengembalikan records
                # hrd_ids = self.env['hr.employee'].search([]).mapped('email') #ini mengembalikan array email

                template_obj = self.env['mail.mail']
                template_data = {}
                template_data["subject"] = "Pengajuan Cuti Anda ditolak"
                # template_data["email_from"] = "_default"
                template_data["email_to"] = record.employee_id.work_email
                template_data['body_html'] = """
                    <div style="font-family: 'Lucica Grande', Ubuntu, Arial, Verdana, sans-serif; font-size: 12px; color: rgb(34, 34, 34); background-color: #FFF; ">
                        <p>
                            Dear <strong>%s,</strong>
                            Maaf, Permintaan Cuti Anda <strong>Ditolak</strong> 
                        </p>
                        <p><strong>Description:</strong></p>
                        <p>Leave : %s </p>
                        <p>No of Days : <strong>%s</strong></p>
                        <p>Leave From : %s </p>
                        <p>Leave UpTo : %s </p>
                        <p>Untuk melihat detail dari pengajuan tersebut, silahkan klik link di bawah ini:</p>
                        <p>
                            <a href="http://localhost:8069/web?db=%s#id=%s&view_type=form&model=hr.holidays">Link</a>
                        </p>
                    </div>
                """ % (record.employee_id.name
                        , record.holiday_status_id.name
                        , record.number_of_days_temp
                        , record.date_from
                        , record.date_to
                        , record.env.cr.dbname, record.id)
                kirim_email = template_obj.create(template_data).send()
                a = "kirim email Permintaan Ditolak to: %s  (subject: %s):" % (record.employee_id.work_email, template_data["subject"])
                ff.write(a + "status: " + str(kirim_email) + "\n")

            # ir_model_data = self.pool.get('ir.model.data')
            # template_id = ir_model_data.get_object_reference('leave_notification', 'template_leave_approved')[1]
            # self.pool.get('mail.template').send_mail(template_id, ids[0], force_send=True, context=context)

        return result
