from odoo import models, fields, api

class Course(models.Model):
    _inherit = 'open2.course' #ini adalah nama model parent, yang akan kita inherits

    harga = fields.Float(string="Harga Kursus", default=1000000)
    is_non_aktif = fields.Boolean(default=False)

##############SKENARIO BELAJAR INHERITANCE
# 1. kita akan menambah field Harga di table course
# 2. menginherit view


    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Course, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if(view_type=="form"):
            res['arch'] = """
                    <form string="Course Form">
                        <sheet>
                            <group>
                                <group>
                                    <field name="nama"/>
                                    <field name="harga"/>
                                    <field name="is_non_aktif"/>
                                </group>
                                <group>
                                    <field name="description"/>
                                </group>
                            </group>
                        </sheet>
                    </form>
            """
        elif(view_type=="tree"):
            res['arch'] = """
                <tree string="Course Tree">
                    <field name="nama"/>
                    <field name="harga"/>
                    <field name="is_non_aktif"/>
                    <field name="create_date"/>
                </tree>
            """
        from lxml import etree
        doc = etree.XML(res['arch'])
        View = self.env['ir.ui.view'].sudo()
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res

