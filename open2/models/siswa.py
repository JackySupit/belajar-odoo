from odoo import models, fields, api
import random

class Siswa(models.Model):
    _name = "open2.siswa"

    name = fields.Char(required=True)
    # session_id = fields.Many2one("open2.session")
    session_ids = fields.Many2many("open2.session")

    @api.model
    def create(self, vals):
        angka_random = random.randint(1, 21) * 5
        vals["name"] = vals["name"] + " random on create : " + str(angka_random)
        return super(Siswa, self).create(vals)

    @api.multi
    def write(self, vals):
        angka_random = random.randint(1, 21) * 5
        vals["name"] = vals["name"] + " random on write : " + str(angka_random)
        return super(Siswa, self).write(vals)
