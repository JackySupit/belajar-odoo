from odoo import models, fields, api

class Course(models.Model):
    _name = 'open2.course' ### nama_module.nama_model
    # ## table: open2_course
    # id auto increment int
    # name char 250

#default string = Camel Case, Space

    ##Nama Lengkap
    _rec_name = "nama"

    nama = fields.Char(string="Nama", required=True)
    description = fields.Text()
