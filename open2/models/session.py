from odoo import models, fields, api, exceptions
from datetime import timedelta, datetime

class Session(models.Model):
    _name = 'open2.session'

    name = fields.Char(required=True)
    start_date = fields.Date()
    duration = fields.Float(digits=(6, 2), help="Duration in days")
    seats = fields.Integer(string="Number of seats")
    kursus_id = fields.Many2one("open2.course")
    # siswa_ids = fields.One2many("open2.siswa", "session_id")
    siswa_ids = fields.Many2many("open2.siswa")

    taken_seats = fields.Float(string="Taken seats", compute='_taken_seats')
    instructor_id = fields.Many2one('res.partner', string="Instructor",
                        domain=[("is_instructor", "=", True)])
    end_date = fields.Date(string="End Date", store=True,
                           compute='_get_end_date', inverse='_set_end_date',
                           default=datetime.now())

    state = fields.Selection([
        ('draft', "Draft"),
        ('confirmed', "Confirmed"),
        ('done', "Done"),
    ], default='draft')

    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'

    @api.multi
    def action_done(self):
        self.state = 'done'

    @api.depends('seats', 'siswa_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0.0
                r.available_seats = 0.0
            else:
                r.taken_seats = len(r.siswa_ids)
                r.available_seats = r.seats - r.taken_seats

    available_seats = fields.Float(string="Available seats", compute=_taken_seats)

    @api.onchange('seats', 'siswa_ids')
    def _verify_valid_seats(self):
        if self.seats < 0:
            return {
                'warning': {
                    'title': "Incorrect 'seats' value",
                    'message': "The number of available seats may not be negative",
                },
            }
        if self.seats < len(self.siswa_ids):
            return {
                'warning': {
                    'title': "Too many attendees",
                    'message': "Increase seats or remove excess attendees",
                },
            }

    @api.constrains('seats', 'siswa_ids')
    def _batasi_taken_seats(self):
        for r in self:
            if len(r.siswa_ids):
                if (r.seats == 0) or (len(r.siswa_ids) > r.seats ):
                    raise exceptions.ValidationError("Jumlah Siswa melebihi jumlah kursi tersedia")
                elif not r.seats: #empty / null
                    raise exceptions.ValidationError("Jumlah Seats tidak boleh kosong")

    #1 sesi = cuma punya 1 kursus (Many2one)
    #1 sesi = ada banyak siswa      (One2many)

    #
    # Odoo
    # model inheritance itu ada 2 macam
    # 1. membuat object baru berdasarkan parent
    # 2. merubah parent
    #
    #
    # view inheritance
    # 1. mereplace view yang sudah ada
    # 2. meng-inject view yang sudah ada



    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Session, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if(view_type=="tree"):
            res['arch'] = """
                <tree string="Course Tree">
                    <field name="name"/>
                    <field name="kursus_id"/>
                    <field name="siswa_ids"/>
                    <field name="state"/>
                </tree>
            """
        elif(view_type=="form"):
            res['arch'] = """
                <form string="Session Form">
                    <header>
                        <button name="action_draft" type="object"
                                string="Reset to draft"
                                states="confirmed,done"/>
                        <button name="action_confirm" type="object"
                                string="Confirm" states="draft"
                                class="oe_highlight"/>
                        <button name="action_done" type="object"
                                string="Mark as done" states="confirmed"
                                class="oe_highlight"/>
                        <field name="state" widget="statusbar" clickable="1" />
                    </header>
                    <sheet>
                        <group>
                            <group>
                                <field name="name"/>
                                <field name="start_date"/>
                                <field name="duration"/>
                                <field name="seats"/>
                            </group>
                            <group>
                                <field name="kursus_id"/>
                                <field name="taken_seats"/>
                            </group>
                        </group>
                        <group>
                            <field name="siswa_ids"/>
                        </group>
                    </sheet>
                </form>
            """
        elif (view_type == "calendar"):
            res['arch'] = """
                    <calendar string="Session Calendar" date_start="start_date"
                              date_delay="5">
                        <field name="name"/>
                    </calendar>
                """
        from lxml import etree
        doc = etree.XML(res['arch'])
        View = self.env['ir.ui.view'].sudo()
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res
