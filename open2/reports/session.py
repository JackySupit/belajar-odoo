from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx

class SessionXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, models):
        for rec in models:
            report_name = rec.name
            # One sheet by session
            sheet = workbook.add_worksheet(report_name[:31])
            bold = workbook.add_format({'bold': True})
            hleft = workbook.add_format({'align': 'left'})


            # sheet.write(row, col, value)
            sheet.set_column(0, 0, 50)
            sheet.write(0, 0, rec.name, bold)

            sheet.set_column(0, 1, 30)
            sheet.write(0, 1, rec.start_date, hleft)

            sheet.set_column(0, 2, 20)
            sheet.write(0, 2, rec.seats, hleft)

            sheet.set_column(0, 3, 30)
            sheet.write(0, 3, str(rec.taken_seats) + " siswa", hleft)


SessionXlsx('report.open2.session.xlsx','open2.session')