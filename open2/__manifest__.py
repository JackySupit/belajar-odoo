# -*- coding: utf-8 -*-
{
    'name': "Open 2",

    'summary': """Summary open 2""",

    'description': """
        Open Academy module for managing trainings:
            - training courses
            - training sessions
            - attendees registration
    """,

    'author': "Jukecloud",
    'website': "http://www.jukesolutions.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Test',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','report', 'report_xlsx'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'templates.xml',
        'views/course.xml',
        'views/session.xml',
        'views/siswa.xml',
        'views/wizards/wizard_session_state.xml',
        'views/reports/session.xml',
        'views/menu.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo.xml',
    ],
    'installable': True,
    'application': True
}