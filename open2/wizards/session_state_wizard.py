# -*- coding: utf-8 -*-
from odoo import models, fields, api
class SessionState(models.TransientModel):
    _name = 'open2.wizard_session_state'

    def _default_session(self):
        return self.env['open2.session'].search([("state", "!=", "done")])

    session_ids = fields.Many2many('open2.session', string="Session", required=True, default=_default_session)

    @api.model
    def create(self, vals):
        ids = []
        # import ipdb; ipdb.set_trace()
        for satu in vals["session_ids"]:
            ids.append(satu[2][0])
        recs = self.env['open2.session'].search([("id", "in", ids)])
        recs.write({"state":"done"})

        # munculkan pesan sukses, reload page
        return self