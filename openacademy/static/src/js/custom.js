odoo.define('openacademy.custom', function (require) {
"use strict";
    $(document).ready(function() {
        var group_siswa_1 = $(document).find(".group_siswa_1");
        var form_input_name = group_siswa_1.find(".form_input_name");
        var current_name = form_input_name.val();
        var o_horizontal_separator = group_siswa_1.find(".o_horizontal_separator");
        if(current_name) {
            o_horizontal_separator.text("Edit Siswa: " + current_name);
        }
    })
});