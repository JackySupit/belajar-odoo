# -*- coding: utf-8 -*-
from odoo import fields, models, api

class Partner(models.Model):
    _inherit = 'res.partner'

    # Add a new column to the res.partner model, by default partners are not
    # instructors

    is_instructor = fields.Boolean("Is This An Instructor", default=False)
    instructor_description = fields.Char("Instructor Description", default="This is a default instructor description")

    session_ids = fields.Many2many('openacademy.session',
        string="Attended Sessions", readonly=False)

    # session_ids = fields.Many2many('openacademy.session',
    #     string="Attended Sessions", readonly=True)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Partner, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        View = self.env['ir.ui.view'].sudo()
        from lxml import etree
        if res.get('view_id'):
           view_id = res['view_id']
        if view_id:
           root_view = View.browse(view_id).read_combined(['arch'])
           res['arch'] = root_view['arch']
        doc = etree.XML(res['arch'])
        # for node in doc.xpath("//page[@string='Internal Notes']"):
        for node in doc.xpath("//notebook/page"):
            node.addprevious(etree.XML("""
                    <page string="Sessions Tab 4">
                        <group>
                            <group>
                                <field name="is_instructor"/>
                                <field name="session_ids"/>
                            </group>
                            <group>
                                <field name="instructor_description"/>
                            </group>
                        </group>
                    </page>
                    <!--
                    <script type="text/javascript" src="/openacademy/static/src/js/custom.js"></script>
                    -->
            """))
            break
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res