# -*- coding: utf-8 -*-
from odoo import models, fields, api
import logging
_logger = logging.getLogger(__name__)

class Siswa(models.Model):
    _name = 'openacademy.siswa'

    name = fields.Char(string="Title", required=True)
    birth_date = fields.Date(string="Date of birth")
    birth_city = fields.Char(string="City of birth")

    description = fields.Text()

    course_ids = fields.Many2many('openacademy.course', string="Course")
    status_menikah = fields.Selection([("belum_menikah", "Belum Menikah"), ("menikah", "Menikah")], default="belum_menikah")
    
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False, **kwargs):
        res = super(Siswa, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

        params = self._context.get('params')
        nama = 'no name'
        if(params):
            id_siswa = params.get('id')
            _logger.warn("id_siswa: " + str(id_siswa))
            # import ipdb;ipdb.set_trace();
            rec_siswa = self.env['openacademy.siswa'].browse(id_siswa)
            nama = rec_siswa.name


        if(view_type=="form"):
            
            is_punya_hak_akses = False
            if(is_punya_hak_akses):
                is_clickable = "True"
                is_readonly = False
                string_clickable = 'clickable="True"'
            else:
                is_clickable = "False"
                is_readonly = True
                string_clickable = ''

            # import ipdb; ipdb.set_trace();
            res['arch'] = """
                    <form string="Form Siswa" class="group_siswa_1">
                        <header>
                            <field name="status_menikah" widget="statusbar" %s/>
                        </header>
                        <sheet>
                            <group String="Siswa" >
                                <field name="name" class="form_input_name"/>
                                <field name="birth_date" class="form_input_birth_date"/>
                                <field name="birth_city" class="form_input_birth_city"/>
                            </group>
                            <notebook>
                                <page string="Course">
                                    <label for="course_ids"/>
                                    <field name="course_ids"/>
                                </page>
                                <page string="Description">
                                    <field name="description"/>
                                </page>
                                <page string="About">
                                    This is an example of notebooks
                                </page>
                            </notebook>
                        </sheet>
                    </form>
            """ % (string_clickable)

        elif(view_type=="tree"):
            res['arch'] = """
                <tree string="List Siswa">
                    <field name="name"/>
                    <field name="birth_date"/>
                    <field name="birth_city"/>
                    <field name="status_menikah"/>
                    <field name="description"/>
                </tree>
            """
        from lxml import etree
        doc = etree.XML(res['arch'])
        View = self.env['ir.ui.view'].sudo()
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res