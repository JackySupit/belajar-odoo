# -*- coding: utf-8 -*-
from odoo import fields, models, api

class Pramugari(models.Model):
    _inherit = 'res.partner'

    # Add a new column to the res.partner model, by default partners are not
    # instructors

    is_pramugari = fields.Boolean("Is Pramugari", default=False)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Pramugari, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        View = self.env['ir.ui.view'].sudo()
        from lxml import etree
        if res.get('view_id'):
           view_id = res['view_id']
        if view_id:
           root_view = View.browse(view_id).read_combined(['arch'])
           res['arch'] = root_view['arch']
        doc = etree.XML(res['arch'])
        # for node in doc.xpath("//page[@string='Internal Notes']"):
        for node in doc.xpath("//field[@name='category_id']"):
            node.addprevious(etree.XML("""
                <field name="is_pramugari"/>
            """))
            break
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res