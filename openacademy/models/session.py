from datetime import timedelta, datetime
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError
# import os.path
import os
import sys
class Session(models.Model):
    _name = 'openacademy.session'

    name = fields.Char(required=True)
    start_date = fields.Date()
    seats = fields.Integer(string="Number of seats", required=True)

    duration = fields.Float(digits=(2,0), help="Duration in hour", default=0)
    price = fields.Float(digits=(6,0), help="Price Per Hour", default=0)
    total_price = fields.Float(help="Duration x Price", compute="_onchange_total_price")

    instructor_id = fields.Many2one('res.partner', string="Instructor",
                        domain=[("is_instructor", "=", True)])
    course_id = fields.Many2one('openacademy.course', ondelete='cascade', string="Course", required=False)

    attendee_ids = fields.Many2many('res.partner', string="Attendees")
    taken_seats = fields.Float(string="Taken seats", compute='_taken_seats')
    
    end_date = fields.Date(string="End Date", store=True,
        compute='_get_end_date', inverse='_set_end_date', 
        default=datetime.now())


    @api.depends('start_date', 'duration')
    def _get_end_date(self):
        for r in self:
            if not (r.start_date and r.duration):
                r.end_date = r.start_date
                continue

            # Add duration to start_date, but: Monday + 5 days = Saturday, so
            # subtract one second to get on Friday instead
            start = fields.Datetime.from_string(r.start_date)
            duration = timedelta(days=r.duration, seconds=-1)
            r.end_date = start + duration

    def _set_end_date(self):
        for r in self:
            if not (r.start_date and r.end_date):
                continue

            # Compute the difference between dates, but: Friday - Monday = 4 days,
            # so add one day to get 5 days instead
            start_date = fields.Datetime.from_string(r.start_date)
            end_date = fields.Datetime.from_string(r.end_date)
            r.duration = (end_date - start_date).days + 1

    @api.constrains('instructor_id', 'attendee_ids')
    def _check_instructor_not_in_attendees(self):
        for r in self:
            if r.instructor_id and r.instructor_id in r.attendee_ids:
                # row = self.pool.get('res.partner').browse(cr,uid,name2.id,context=context)
                nama = r.instructor_id.name
                pesan = _("A session's instructor can't be an attendee: ") + nama
                raise exceptions.ValidationError(pesan)


    @api.depends('seats', 'attendee_ids')
    def _taken_seats(self):
        for r in self:
            if not r.seats:
                r.taken_seats = 0.0
            else:
                r.taken_seats = 100.0 * len(r.attendee_ids) / r.seats


    @api.onchange("duration", "price")
    def _onchange_total_price(self):
        for rec in self:
            if(rec.duration >0 and rec.price > 0):
                rec.total_price = rec.duration * rec.price
            # else:    
            #     return {
            #         'warning': {
            #             'title': "Duration, Price Problem",
            #             'message': "something went wrong with Duration and Price",
            #         },
            #     }

    @api.constrains('seats')
    def _check_seats_constrains2(self):
        if self.seats < 3:
            pesan = _("Seats minimal 3 (tiga)")
            raise ValidationError(pesan)


    @api.constrains('seats', 'attendee_ids')
    def _check_seats_constrains(self):
        if self.seats <= 0 or self.seats < len(self.attendee_ids):
            pesan = _('Values must be greater than 0 and greather than the Attendences: ') + str(len(self.attendee_ids))
            raise ValidationError(pesan)

    # @api.onchange('seats', 'attendee_ids')
    # def _check_seats_onchange(self):
    #     if self.seats <= 0 or self.seats < len(self.attendee_ids):
    #         pesan = "Jumlah seats minimal: " + str(len(self.attendee_ids))
    #         # raise Warning(pesan) #error

    #         raise exceptions.ValidationError(pesan) ##tetep tersimpan

    #         #tetep tersimpan 
    #         # return {
    #         #     'warning': {
    #         #         'title': "Jumlah Peserta terlalu banyak",
    #         #         'message': pesan,
    #         #     },
    #         # }

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):

        save_path = '/var/log/odoo/'
        name_of_file = "custom-log.log"
        file_path = save_path + name_of_file

        # fobj = open(file_path, 'w')
        # fobj.write('test 1\n')
        # fobj.write('test 2\n')
        # fobj.write('test 3\n')
        # fobj.write('test 4')
        # fobj.close()

        # completeName = os.path.join(save_path, name_of_file + ".log")
        # file1 = open(completeName, "w")
        # toFile = raw_input("view_type A: " + view_type);
        #
        # file1.write(toFile)
        # file1.close()

        open(file_path, 'a').write("---------------------------\n")
        open(file_path, 'a').write('Testing A: view type: ' + view_type + "\n")

        res = super(Session, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

        open(file_path, 'a').write('Testing B: view type: ' + view_type + "\n")
        open(file_path, 'a').write("---------------------------\n")


        open(file_path, 'a').write(res['arch'])
        open(file_path, 'a').write("---------------------------\n")

        # toFile = raw_input("view_type B: " + view_type);
        # file1.write(toFile)
        # file1.close()

        if(view_type=="form"):
            res['arch'] = """
                <form string="Course Form">
                    <sheet>
                        <group>
                            <group string="General">
                                <field name="course_id"/>
                                <field name="name"/>
                                <field name="instructor_id"/>
                            </group>
                            <group string="Schedule">
                                <field name="seats"/>
                                <field name="taken_seats" widget="progressbar"/>
                            </group>
                        </group>
                        <group>
                            <group>
                                <field name="start_date"/>
                                <field name="duration"/>

                                <field name="end_date" readonly="1"/>
                                <field name="price"/>
                            </group>
                            <group>
                                <field name="total_price"/>
                            </group>
                        </group>
                        <notebook>
                            <page String="Attendences">
                                <label for="attendee_ids"/>
                                <field name="attendee_ids"/>
                            </page>
                        </notebook>
                    </sheet>
                </form>
            """
        elif(view_type=="tree"):
            res['arch'] = """
                <tree string="Session Tree"  decoration-info="duration&lt;5"   decoration-warning="duration&gt;6 and duration&lt;11" decoration-danger="duration&gt;10">
                    <field name="name"/>
                    <field name="course_id"/>
                    <field name="start_date"/>
                    <field name="end_date"/>
                    <field name="duration" invisible="0"/>
                    <field name="price"/>
                    <field name="total_price"/>
                    <field name="taken_seats" widget="progressbar"/>
                </tree>
            """
        elif(view_type=="calendar"):
            res['arch'] = """
                <calendar string="Session Calendar" date_start="start_date"
                          date_stop="end_date"
                          color="instructor_id">
                    <field name="name"/>
                </calendar>
            """
            open(file_path, 'a').write(res['arch'])
            open(file_path, 'a').write("---------------------------\n")

            import ipdb; ipdb.set_trace();
            open(file_path, 'a').write(res)
            open(file_path, 'a').write("---------------------------\n")

        from lxml import etree
        doc = etree.XML(res['arch'])
        View = self.env['ir.ui.view'].sudo()
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res
