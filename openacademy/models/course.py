# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError
import re
import logging
_logger = logging.getLogger(__name__)

class Course(models.Model):
    _name = 'openacademy.course'
    
    code = fields.Char(string="Code", required=True)
    name = fields.Char(string="Title 2", required=True)
    description = fields.Text()

    responsible_id = fields.Many2one('res.users',
        ondelete='set null', string="Responsible", index=True)
    
    session_ids = fields.One2many(
        'openacademy.session', 'course_id', string="Sessions")

    siswa_ids = fields.Many2many('openacademy.siswa', string="Siswa")



    @api.multi
    def copy(self, default=None):
        default = dict(default or {})

        i = 1
        string_increment = "Copy {} of".format(i)

        old_code = self.code;
        old_name = self.name;
        pattern = re.compile(r"Copy \d+ of")

        is_this_is_already_a_copy = bool(re.match(pattern, old_name))
        if(is_this_is_already_a_copy):
            findCopyName = re.findall(pattern, old_name)
            _logger.warning(findCopyName)

            pattern_number = re.compile(r'/\d+')
            current_number = [int(s) for s in re.findall(pattern_number, findCopyName[0])]
            _logger.warning(_("is_this_is_already_a_copy: ") +  str(is_this_is_already_a_copy) + " | copy of: " + str(current_number) )

            old_name = pattern.sub("", old_name).strip()
            _logger.warning("old_name: " + old_name)

            old_code = pattern.sub("", old_code).strip()
            _logger.warning("old_code: " + old_code)


            

        new_name = "{} {}".format(string_increment, old_name)
        new_code = "{} {}".format(string_increment, old_code)
        copied_count = self.search_count(
                [('name', '=like', new_name)]
            )
        if copied_count:
            while copied_count:
                i += 1
                string_increment = "Copy {} of".format(i)
                new_name = "{} {}".format(string_increment, old_name)
                new_code = "{} {}".format(string_increment, old_code)
                copied_count = self.search_count(
                    [('name', '=like', new_name)]
                )
                _logger.warning(string_increment)
        default['name'] = new_name
        default['code'] = new_code

        return super(Course, self).copy(default)

    _sql_constraints = [
        ('name_description_check',
         'CHECK(name != description)',
         _("The title of the course should not be the description")),
        ('unique_name',
         'UNIQUE(name)',
         _("The course Title must be unique")),
        ('unique_code',
         'UNIQUE(code)',
         _("The course Code must be unique")),
    ]

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(Course, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if(view_type=="form"):
            res['arch'] = """
                    <form string="Course Form">
                        <sheet>
                            <group>
                                <group>
                                    <field name="code"/>
                                    <field name="responsible_id"/>
                                </group>
                                <group>
                                    <field name="name"/>
                                </group>
                            </group>
                            <notebook>
                                <page string="Siswa">
                                    <label for="siswa_ids"/>
                                    <field name="siswa_ids"/>
                                </page>
                                <page string="Description">
                                    <field name="description"/>
                                </page>
                                <page string="Sessions">
                                    <field name="session_ids">
                                        <tree string="Registered sessions">
                                            <field name="name"/>
                                            <field name="instructor_id"/>
                                        </tree>
                                    </field>
                                </page>
                                <page string="About">
                                    This is an example of notebooks
                                </page>
                            </notebook>
                        </sheet>
                    </form>
            """    
        elif(view_type=="tree"):
            res['arch'] = """
                <tree string="Course Tree">
                    <field name="code"/>
                    <field name="name"/>
                    <field name="responsible_id"/>
                    <field name="create_date"/>
                </tree>
            """ 
        elif(view_type=="search"):
            res['arch'] = """
                <search>
                    <field name="name"/>
                    <field name="description"/>
                    <filter name="my_courses" string="My Courses"
                            domain="[('responsible_id', '=', uid)]"/>
                    <group string="Group By">
                        <filter name="by_responsible" string="Responsible"
                                context="{'group_by': 'responsible_id'}"/>
                    </group>
                </search>
            """
        from lxml import etree
        doc = etree.XML(res['arch'])
        View = self.env['ir.ui.view'].sudo()
        res['arch'], res['fields'] = View.postprocess_and_fields(self._name, doc, view_id)
        return res

